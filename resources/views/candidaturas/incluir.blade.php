@extends('layouts.app')

@section('titulo')
    Incluir candidatura
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="container-fluid">
            <!-- Example DataTables Card-->
            <div class="card mb-3">
                <div class="card-header">
                    <strong>@yield('titulo')</strong>
                    <a href="{{ route('listarCandidaturas') }}" title="Voltar"><button class="btn btn-dark float-right btn-sm"><i class="fa fa-undo"></i></button></a>
                </div>
                <div class="card-body">
                    @if($errors->any())
                        <div class="alert alert-warning alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            {{$errors->first()}}
                        </div>
                    @endif
                    <form method="post" action="{{ route('salvarCandidatura') }}" class="" enctype="multipart/form-data">
                        @csrf
                        @include('candidaturas.form')
                        <button type="submit" class="btn btn-success btn-block">Confirmar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
