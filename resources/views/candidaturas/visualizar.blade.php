@extends('layouts.app')

@section('titulo')
    Candidaturas
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="container-fluid">
            <!-- Example DataTables Card-->
            <div class="card mb-3">
                <div class="card-header">
                    <strong>@yield('titulo')</strong>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="tabela" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Matrícula</th>
                                <th>Nome</th>
                                <th>Departamento</th>
                                <th>Disciplina</th>
                                <th>Histórico</th>
                                <th>Data de Candidatura</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Matrícula</th>
                                <th>Nome</th>
                                <th>Departamento</th>
                                <th>Disciplina</th>
                                <th>Histórico</th>
                                <th>Data de Candidatura</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($candidaturas as $candidatura)
                                <tr>
                                    <td>{{$candidatura->getAluno->label}}</td>
                                    <td>{{$candidatura->getAluno->name}}</td>
                                    <td>{{$candidatura->getDisciplina->getDepartamento->nome}}</td>
                                    <td>{{$candidatura->getDisciplina->nome}}</td>
                                    <td class="text-center">
                                        <a target="_blank" href="{{asset("storage/".$candidatura->historico)}}" title="Baixar histórico"><button class="btn btn-danger btn-sm"><i class="fa fa-file-pdf-o"></i> PDF</button></a>
                                    </td>
                                    <td>{{date_format($candidatura->created_at,'d/m/Y')}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
