<div class="form-group">
    <div class="form-row">
        <div class="col-md-2">
            <label for="matricula">Matrícula</label>
            <input type="text" id="matricula" name="matricula" value="{{ Auth::user()->label }}" required="required" class="form-control" readonly="readonly">
        </div>
        <div class="col-md-10">
            <label for="nome">Nome</label>
            <input type="text" id="nome" name="nome" value="{{ Auth::user()->name }}" required="required" class="form-control" readonly="readonly">
        </div>
    </div>
</div>
<div class="form-group">
    <div class="form-row">
        <div class="col-md-12">
            <label for="departamento">Departamento</label>
            <select id="departamento" name="departamento" required="required" autofocus="autofocus" class="form-control">
                @foreach($departamentos as $departamento)
                    <option @isset($candidatura) @if ($candidatura->departamento == $departamento->id) selected @endif @endisset value="{{$departamento->id}}">{{$departamento->nome}} ({{$departamento->codigo}})</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="form-group">
    <div class="form-row">
        <div class="col-md-12">
            <label for="disciplina">Disciplina</label>
            <select id="disciplina" name="disciplina" required="required" autofocus="autofocus" class="form-control">
                @foreach($disciplinas as $disciplina)
                    <option @isset($candidatura) @if($candidatura->disciplina == $disciplina->id) selected @endif @endisset value="{{$disciplina->id}}">{{$disciplina->nome}} ({{$disciplina->codigo}})</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="form-group">
    <div class="form-row">
        <div class="col-md-3">
                <label for="voluntario" class="btn-radio">
                    <input type="radio" id="voluntario" value="voluntario" name="tipo" @isset($candidatura) @if($candidatura->voluntario == 1) checked @endif @endisset>
                    <svg width="20px" height="20px" viewBox="0 0 20 20">
                        <circle cx="10" cy="10" r="9"></circle>
                        <path d="M10,7 C8.34314575,7 7,8.34314575 7,10 C7,11.6568542 8.34314575,13 10,13 C11.6568542,13 13,11.6568542 13,10 C13,8.34314575 11.6568542,7 10,7 Z" class="inner"></path>
                        <path d="M10,1 L10,1 L10,1 C14.9705627,1 19,5.02943725 19,10 L19,10 L19,10 C19,14.9705627 14.9705627,19 10,19 L10,19 L10,19 C5.02943725,19 1,14.9705627 1,10 L1,10 L1,10 C1,5.02943725 5.02943725,1 10,1 L10,1 Z" class="outer"></path>
                    </svg>
                    <span>Voluntário</span>
                </label>
                <label for="bolsista" class="btn-radio">
                    <input type="radio" id="bolsista" value="bolsista" name="tipo" @isset($candidatura) @if($candidatura->voluntario == 0) checked @endif @endisset>
                    <svg width="20px" height="20px" viewBox="0 0 20 20">
                        <circle cx="10" cy="10" r="9"></circle>
                        <path d="M10,7 C8.34314575,7 7,8.34314575 7,10 C7,11.6568542 8.34314575,13 10,13 C11.6568542,13 13,11.6568542 13,10 C13,8.34314575 11.6568542,7 10,7 Z" class="inner"></path>
                        <path d="M10,1 L10,1 L10,1 C14.9705627,1 19,5.02943725 19,10 L19,10 L19,10 C19,14.9705627 14.9705627,19 10,19 L10,19 L10,19 C5.02943725,19 1,14.9705627 1,10 L1,10 L1,10 C1,5.02943725 5.02943725,1 10,1 L10,1 Z" class="outer"></path>
                    </svg>
                    <span>Bolsista</span>
                </label>
        </div>
    </div>

    <div class="form-group">
        <div class="form-row">
            <div class="col-md-12">
                <label for="file-upload" class="custom-file-upload">
                    <i class="fa fa-paperclip"></i> Histórico
                </label>
                 <input id="file-upload" type="file" accept=".pdf" onchange="atualizarNomeArquivo();" name="file"/> <span class="btn-link" id="fileName">@isset($candidatura) <a target="_blank" href="{{ asset("storage/".$candidatura->historico)}}" title="Baixar histórico"><button class="btn btn-danger btn-sm" type="button"><i class="fa fa-file-pdf-o"></i> PDF</button></a> @endisset</span>
            </div>
        </div>
    </div>


</div>
