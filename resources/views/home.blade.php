@extends('layouts.app')

@section('titulo')
    Início
@endsection

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="card mb-3">
            <div class="card-header">
                <strong>@yield('titulo')</strong>
            </div>
            <div class="card-body">
              @if (session('status'))
                  <div class="alert alert-success">
                      {{ session('status') }}
                  </div>
              @endif
              Olá, {{Auth::user()->name}}.
              <br/><br/>
              <div class="alert alert-primary" role="alert">
                Programa de Monitoria: <a href="mailto:programademonitoria@cefet-rj.br" class="alert-link">programademonitoria@cefet-rj.br</a>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
