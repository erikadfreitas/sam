<!DOCTYPE html>
<html lang="pt">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sb-admin.css') }}" rel="stylesheet">
</head>

<body class="bg-dark">
<div class="container">
    <h1 class="text-center text-white" style="padding-top: 30px;"><i class="fa fa-graduation-cap"></i> {{ config('app.name') }}</h1>
    <div class="card card-login mx-auto mt-5">
        <div class="card-header">Login</div>
        <div class="card-body">
            <form method="POST" action="{{ route('login') }}">
                @csrf

                <div class="form-group">
                  <label for="label"><i class="fa fa-id-card"></i></label>
                  <input id="label" type="text" placeholder="Matrícula" class="form-control" name="label" required autofocus>
                  @if ($errors->has('label'))
                      <span class="invalid-feedback">
                                  <strong>{{ $errors->first('label') }}</strong>
                      </span>
                  @endif
                </div>

                <div class="form-group">
                    <label for="password"><i class="fa fa-unlock-alt"></i></label>
                    <input id="password" type="password" placeholder="Senha" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                    @if ($errors->has('password'))
                        <span class="invalid-feedback">
                                    <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Lembrar minha senha</label>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-block">
                    Acessar
                </button>

            </form>
            <div class="text-center">
                <a class="d-block small mt-3" href="{{ route('register') }}">Criar uma conta</a>
                {{--<a class="d-block small" href="{{ route('password.request') }}">Esqueci minha senha</a>--}}
            </div>
        </div>
    </div>
</div>
<!-- Bootstrap core JavaScript-->
<script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- Core plugin JavaScript-->
<script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>
</body>

</html>
