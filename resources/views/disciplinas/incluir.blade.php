@extends('layouts.app')

@section('titulo')
    Incluir disciplina
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="container-fluid">
            <!-- Example DataTables Card-->
            <div class="card mb-3">
                <div class="card-header">
                    <strong>@yield('titulo')</strong>
                    <a href="{{ route('listarDisciplinas') }}" title="Voltar"><button class="btn btn-dark float-right btn-sm"><i class="fa fa-undo"></i></button></a>
                </div>
                <div class="card-body">
                    <form method="post" action="{{ route('salvarDisciplina') }}" class="">
                        @csrf
                        @include('disciplinas.form')
                        <button type="submit" class="btn btn-success btn-block">Confirmar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection