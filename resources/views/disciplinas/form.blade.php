<div class="form-group">
    <div class="form-row">
        <div class="col-md-2">
            <label for="codigo">Código</label>
            <input type="text" id="codigo" name="codigo" value="@isset($disciplina){{$disciplina->codigo}}@endisset" required="required" autofocus="autofocus" class="form-control">
        </div>
        <div class="col-md-5">
            <label for="nome">Nome</label>
            <input type="text" id="nome" name="nome" value="@isset($disciplina){{$disciplina->nome}}@endisset" required="required" autofocus="autofocus" class="form-control">
        </div>
        <div class="col-md-5">
            <label for="professor">Professor</label>
            <input type="text" id="professor" name="professor" value="@isset($disciplina){{$disciplina->professor}}@endisset" required="required" autofocus="autofocus" class="form-control">
        </div>
    </div>
</div>
<div class="form-group">
    <div class="form-row">
        <div class="col-md-10">
            <label for="departamento">Departamento</label>
            <select id="departamento" name="departamento" required="required" autofocus="autofocus" class="form-control">
                @foreach($departamentos as $departamento)
                    <option @isset($disciplina) @if($disciplina->departamento == $departamento->id) selected="selected" @endif @endisset value="{{$departamento->id}}">{{$departamento->nome}} ({{$departamento->codigo}})</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-1">
            <label for="vagas">Vagas</label>
            <input type="number" id="vagas" name="vagas" min="0" value="@isset($disciplina){{$disciplina->vagas}}@else{{0}}@endisset" required="required" autofocus="autofocus" class="form-control">
        </div>
        <div class="col-md-1">
            <label for="prova">Prova</label><br>
            <input type="checkbox" @isset($disciplina) @if($disciplina->prova == 1) checked="checked" @endif @endisset id="prova" name="prova" autofocus="autofocus" class="form-control" data-toggle="toggle" data-on="Sim" data-off="Não" data-offstyle="light" data-onstyle="dark">
        </div>
    </div>
</div>
