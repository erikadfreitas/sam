@extends('layouts.app')

@section('titulo')
    Documentos
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="card mb-3">
                <div class="card-header">
                    <strong>@yield('titulo')</strong>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Arquivo</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Nome</th>
                                <th>Arquivo</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($documentos as $documento)
                                <tr>
                                    <td>{{$documento->nome}}</td>
                                    <td class="text-center">
                                        <a target="_blank" href="{{asset("storage/".$documento->arquivo)}}" title="Baixar histórico">
                                            <button class="btn btn-dark btn-sm">
                                                <i class="fa fa-download"></i> Baixar
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection