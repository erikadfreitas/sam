@extends('layouts.app')

@section('titulo')
    Incluir documento
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="card mb-3">
                <div class="card-header">
                    <strong>@yield('titulo')</strong>
                    <a href="{{ route('listarDocumentos') }}" title="Voltar"><button class="btn btn-dark float-right btn-sm"><i class="fa fa-undo"></i></button></a>
                </div>
                <div class="card-body">
                    @if($errors->any())
                        <div class="alert alert-warning alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            {{$errors->first()}}
                        </div>
                    @endif
                    <form method="post" action="{{ route('salvarDocumento') }}" class="" enctype="multipart/form-data">
                        @csrf
                        @include('documentos.form')
                        <button type="submit" class="btn btn-success btn-block">Confirmar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection