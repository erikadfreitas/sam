<div class="form-group">
    <div class="form-row">
        <div class="col-md-11">
            <label for="matricula">Nome</label>
            <input type="text" id="nome" name="nome" value="@isset($documento){{$documento->nome}}@endisset" required="required" class="form-control">
        </div>
        <div class="col-md-1">
            <label for="status">Status</label><br>
            <input type="checkbox" @isset($documento) @if($documento->ativo == 1) checked="checked" @endif @endisset id="status" name="status" autofocus="autofocus" class="form-control" data-toggle="toggle" data-on="Ativo" data-off="Inativo" data-offstyle="light" data-onstyle="dark">
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-12">
            <br/>
            <label for="file-upload" class="custom-file-upload">
                <i class="fa fa-paperclip"></i> Arquivo
            </label>
            <input id="file-upload" type="file" onchange="atualizarNomeArquivo();" name="file"/> <span class="btn-link" id="fileName">@isset($documento) <a target="_blank" href="{{ asset("storage/".$documento->arquivo)}}" title="Baixar documento"><button class="btn btn-danger btn-sm" type="button"><i class="fa fa-file-pdf-o"></i> PDF</button></a> @endisset</span>
        </div>
    </div>
</div>
