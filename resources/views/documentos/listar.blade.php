@extends('layouts.app')

@section('titulo')
    Documentos
@endsection

@section('content')
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Deseja excluir o documento?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" id="modalDeleteContent"></div>
            <div class="modal-footer">
                <button class="btn btn-dark" type="button" data-dismiss="modal">Cancelar</button>
                <a class="btn btn-danger" id="modalDeleteConfirm" href="#">Confirmar</a>
            </div>
        </div>
    </div>
</div>

<div class="content-wrapper">
    <div class="container-fluid">
        <div class="card mb-3">
            <div class="card-header">
                <strong>@yield('titulo')</strong>
                <a href="{{ route('incluirDocumento') }}" title="Incluir"><button class="btn btn-success float-right btn-sm"><i class="fa fa-plus"></i></button></a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Arquivo</th>
                            <th>Status</th>
                            <th>Ações</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Nome</th>
                            <th>Arquivo</th>
                            <th>Status</th>
                            <th>Ações</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($documentos as $documento)
                            <tr>
                                <td>{{$documento->nome}}</td>
                                <td class="text-center">
                                    <a target="_blank" href="{{asset("storage/".$documento->arquivo)}}" title="Baixar histórico">
                                        <button class="btn btn-dark btn-sm">
                                            <i class="fa fa-download"></i> Baixar
                                        </button>
                                    </a>
                                </td>
                                <td>@if($documento->ativo == 0) Inativo @else Ativo @endif</td>
                                <td class="text-center">
                                    <a href="{{route('editarDocumento', $documento->id)}}" title="Editar"><button class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button></a>
                                    <a onclick="atualizarModelDocumento('{{$documento->id}}','{{$documento->nome}}');" data-toggle="modal" data-target="#deleteModal" title="Excluir"><button class="btn btn-danger btn-sm"><i class="fa fa-trash-alt"></i></button></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
