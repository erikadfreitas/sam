@extends('layouts.app')

@section('titulo')
    Editar departamento
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="container-fluid">
            <!-- Example DataTables Card-->
            <div class="card mb-3">
                <div class="card-header">
                    <strong>@yield('titulo')</strong>
                    <a href="{{ route('listarDepartamentos') }}" title="Voltar"><button class="btn btn-dark float-right"><i class="fa fa-undo"></i></button></a>
                </div>
                <div class="card-body">
                    <form method="post" action="{{ route('atualizarDepartamento', $departamento->id) }}" class="">
                        @csrf
                        <input type="hidden" name="_method" value="put">
                        @include('departamentos.form')
                        <button type="submit" class="btn btn-success btn-block">Confirmar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection