@extends('layouts.app')

@section('titulo')
    Departamentos
@endsection

@section('content')
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Deseja excluir o departamento?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" id="modalDeleteContent"></div>
            <div class="modal-footer">
                <button class="btn btn-dark" type="button" data-dismiss="modal">Cancelar</button>
                <a class="btn btn-danger" id="modalDeleteConfirm" href="#">Confirmar</a>
            </div>
        </div>
    </div>
</div>

<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Example DataTables Card-->
        <div class="card mb-3">
            <div class="card-header">
                <strong>@yield('titulo')</strong>
                <a href="{{ route('incluirDepartamento') }}" title="Incluir"><button class="btn btn-success float-right btn-sm"><i class="fa fa-plus"></i></button></a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Código</th>
                            <th>Nome</th>
                            <th>Vagas</th>
                            <th>Ações</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Código</th>
                            <th>Nome</th>
                            <th>Vagas</th>
                            <th>Ações</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($departamentos as $departamento)
                            <tr>
                                <td>{{$departamento->codigo}}</td>
                                <td>{{$departamento->nome}}</td>
                                <td>{{$departamento->vagas}}</td>
                                <td class="text-center">
                                    <a href="{{route('editarDepartamento', $departamento->id)}}" title="Editar"><button class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button></a>
                                    <a onclick="atualizarModelDepartamento('{{$departamento->id}}','{{$departamento->codigo}} - {{$departamento->nome}}');" data-toggle="modal" data-target="#deleteModal" title="Excluir"><button class="btn btn-danger btn-sm"><i class="fa fa-trash-alt"></i></button></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection