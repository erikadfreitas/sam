<div class="form-group">
    <div class="form-row">
        <div class="col-md-2">
            <label for="matricula">Código</label>
            <input type="text" id="codigo" name="codigo" value="@isset($departamento){{$departamento->codigo}}@endisset" required="required" class="form-control">
        </div>
        <div class="col-md-9">
            <label for="nome">Nome</label>
            <input type="text" id="nome" name="nome" value="@isset($departamento){{$departamento->nome}}@endisset" required="required" class="form-control">
        </div>
        <div class="col-md-1">
            <label for="vagas">Vagas</label>
            <input type="number" id="vagas" name="vagas" min="0" value="@isset($departamento){{$departamento->vagas}}@else{{0}}@endisset" required="required" autofocus="autofocus" class="form-control">
        </div>
    </div>
</div>
