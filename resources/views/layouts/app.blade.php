<!DOCTYPE html>
<html lang="pt">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>{{ config('app.name') }}</title>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sb-admin.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/bootstrap/css/bootstrap-toggle.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/datatables/buttons.dataTables.min.css') }}" rel="stylesheet">
    {{--<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet">--}}
    <link href="{{ asset('vendor/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">



<body class="fixed-nav sticky-footer bg-dark" id="page-top">
<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="#">
        <i class="fa fa-fw fa-graduation-cap"></i>
        {{ config('app.name', 'Laravel') }}
    </a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
            @if (Auth::user()->type == 1) <!--PROFESSOR-->
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Cadastros">
                    <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#subMenuCadastros">
                        <i class="fa fa-fw fa-bars"></i>
                        <span class="nav-link-text">Cadastros</span>
                    </a>
                    <ul class="sidenav-second-level collapse" id="subMenuCadastros">
                        <li>
                            <a class="nav-link" href="{{route('listarDisciplinas')}}">
                                <i class="fa fa-fw fa-book"></i>
                                <span class="nav-link-text">Disciplinas</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Candidaturas">
                    <a class="nav-link" href="{{route('visualizarCandidaturas')}}">
                    <i class="fa fa-fw fa-clipboard-list"></i>
                    <span class="nav-link-text">Candidaturas</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Documentos">
                    <a class="nav-link" href="{{route('visualizarDocumentos')}}">
                    <i class="fa fa-fw fa-file"></i>
                    <span class="nav-link-text">Documentos</span>
                    </a>
                </li>
            @endif
            @if (Auth::user()->type == 2)
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Minhas Candidaturas">
                    <a class="nav-link" href="{{route('listarCandidaturas')}}">
                    <i class="fa fa-fw fa-clipboard"></i>
                    <span class="nav-link-text">Minhas Candidaturas</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Documentos">
                    <a class="nav-link" href="{{route('visualizarDocumentos')}}">
                        <i class="fa fa-fw fa-file"></i>
                        <span class="nav-link-text">Documentos</span>
                    </a>
                </li>
            @endif
            @if (Auth::user()->type == 3)
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Cadastros">
                    <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#subMenuCadastros">
                        <i class="fa fa-fw fa-bars"></i>
                        <span class="nav-link-text">Cadastros</span>
                    </a>
                    <ul class="sidenav-second-level collapse" id="subMenuCadastros">
                        <li>
                            <a class="nav-link" href="{{route('listarDepartamentos')}}">
                                <i class="fa fa-fw fa-users"></i>
                                <span class="nav-link-text">Departamentos</span>
                            </a>
                        </li>
                        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Documentos">
                            <a class="nav-link" href="{{route('listarDocumentos')}}">
                                <i class="fa fa-fw fa-file"></i>
                                <span class="nav-link-text">Documentos</span>
                            </a>
                        </li>
                    </ul>
                </li>
            @endif
        </ul>
        <ul class="navbar-nav sidenav-toggler">
            <li class="nav-item">
                <a class="nav-link text-center" id="sidenavToggler">
                    <i class="fa fa-fw fa-angle-left"></i>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            @auth
                <li class="nav-item">
                    <a class="nav-link static" style="cursor: default;">
                        Olá, {{ strtok(Auth::user()->name, " ") }}</a>
                </li>
            @endauth
            <!-- <li class="nav-item">
                <a class="nav-link" id="toggleNavColor" title="Inverter cores">
                    <i class="fa fa-fw fa-paint-brush"></i></a>
            </li> -->
            <li class="nav-item">
                <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="fa fa-power-off"></i> Sair
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </ul>
    </div>
</nav>
    @yield('content')
<!-- /.container-fluid-->
<!-- /.content-wrapper-->
<footer class="sticky-footer">
    <div class="container">
        <div class="text-center">
            <small>Copyright &copy; Centro Federal de Educação Tecnológica Celso Suckow da Fonseca - 2018</small>
        </div>
    </div>
</footer>
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fa fa-angle-up"></i>
</a>
<!-- Bootstrap core JavaScript-->
<script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- Core plugin JavaScript-->
<script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js')}} "></script>
<!-- Page level plugin JavaScript-->
<script src="{{ asset('vendor/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/jszip.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('vendor/datatables/buttons.html5.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/buttons.print.min.js')}}"></script>
{{--<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>--}}
<!-- Custom scripts for all pages-->
<script src="{{ asset('js/sb-admin.min.js') }}"></script>
<!-- Custom scripts for this page-->
<script src="{{ asset('js/sb-admin-datatables.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/js/bootstrap-toggle.min.js') }}"></script>


<script>
    $('#toggleNavColor').click(function() {
        $('nav').toggleClass('navbar-dark navbar-light');
        $('nav').toggleClass('bg-dark bg-light');
        $('body').toggleClass('bg-dark bg-light');
    });

    function atualizarModelDisciplina(codigo, detalhes) {
        $("#modalDeleteContent").html("Você está prestes a excluir o seguinte registro: "+detalhes+".");
        $("#modalDeleteConfirm").attr("href", "{{route('deletarDisciplina','codigo')}}".replace("codigo", codigo));
    };

    function atualizarModelDepartamento(codigo, detalhes) {
        $("#modalDeleteContent").html("Você está prestes a excluir o seguinte registro: "+detalhes+".");
        $("#modalDeleteConfirm").attr("href", "{{route('deletarDepartamento','codigo')}}".replace("codigo", codigo));
    };

    function atualizarModelCandidatura(codigo, detalhes) {
        $("#modalDeleteContent").html("Deseja cancelar a candidatura na disciplina "+detalhes+"?");
        $("#modalDeleteConfirm").attr("href", "{{route('deletarCandidatura','codigo')}}".replace("codigo", codigo));
    };

    function atualizarModelDocumento(codigo, detalhes) {
        $("#modalDeleteContent").html("Deseja descadastrar o documento "+detalhes+"?");
        $("#modalDeleteConfirm").attr("href", "{{route('deletarDocumento','codigo')}}".replace("codigo", codigo));
    };

    function atualizarNomeArquivo() {
        let texto = $('#file-upload').prop('files')[0].name;
        $('#fileName').html(texto);
    };

    $('#tabela').DataTable({
        responsive: true,
        dom: 'lBfrtp',
        buttons: [
            {extend: 'copy'},
            // {extend: 'csv'},
            // {extend: 'excel', title: ''},
            {extend: 'pdf', title: ''},
            {extend: 'print',
                customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ]

    });

    $("[type='number']").keypress(function (evt) {
      evt.preventDefault();
    });

</script>
</body>

</html>
