<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidaturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidaturas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('aluno');
            $table->foreign('aluno')->references('id')->on('alunos')->onDelete('restrict');
            $table->integer('disciplina');
            $table->foreign('disciplina')->references('id')->on('disciplinas')->onDelete('restrict');
            $table->enum('status', [1,2,3]);
            $table->boolean('voluntario');
            $table->text('historico');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidaturas');
    }
}
