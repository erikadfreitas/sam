<?php

namespace App\Http\Controllers;

use App\Documento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DocumentoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('perfil.administrativo')->except('visualizar');
    }

    public function listar()
    {
        $documentos = Documento::all();
        return view('documentos.listar', compact('documentos'));
    }

    public function visualizar()
    {
        $documentos = Documento::where('ativo', '=', 1)->get();
        return view('documentos.visualizar', compact('documentos'));
    }

    public function incluir()
    {
        return view('documentos.incluir', compact('documentos'));
    }

    public function salvar(Request $rq)
    {
        $dados = $rq->all();

        if(!$rq->hasFile('file') OR !$rq->file('file')->isValid()) {
            return redirect()->back()->withErrors(['É necessário anexar um arquivo válido.']);
        }

        if($rq->file('file')->getSize() === 0) {
            return redirect()->back()->withErrors(['O arquivo não pode estar vazio.']);
        }

        $historico = $rq->file('file');
        $dados['arquivo'] = $historico->store('documentos');

        if(!$dados['arquivo']){
            return redirect()->back()->withErrors(['Erro no upload do arquivo. Tente novamente.']);
        }

        if(isset($dados['status'])) {
            $dados['ativo'] = TRUE;
        } else {
            $dados['ativo'] = FALSE;
        }

        Documento::create($dados);

        return redirect()->route('listarDocumentos');
    }

    public function editar($id)
    {
        $documento = Documento::find($id);
        return view('documentos.editar', compact('documento'));
    }

    public function atualizar(Request $rq, $id)
    {
        $dados = $rq->all();

        $arquivo = Documento::find($id)->arquivo;

        if($rq->hasFile('file')) {
          if(!$rq->file('file')->isValid()) {
              return redirect()->back()->withErrors(['É necessário anexar um arquivo válido.']);
          }

          if($rq->file('file')->getSize() === 0) {
              return redirect()->back()->withErrors(['O arquivo não pode estar vazio.']);
          }

          $historico = $rq->file('file');
          $dados['arquivo'] = $historico->store('documentos');

          if(!$dados['arquivo']){
              return redirect()->back()->withErrors(['Erro no upload do arquivo. Tente novamente.']);
          }

          Storage::delete($arquivo);
        }

        if(isset($dados['status'])) {
            $dados['ativo'] = TRUE;
        } else {
            $dados['ativo'] = FALSE;
        }

        Documento::find($id)->update($dados);

        return redirect()->route('listarDocumentos');
    }

    public function deletar($id)
    {
        $arquivo = Documento::find($id)->arquivo;
        Documento::find($id)->delete();
        Storage::delete($arquivo);

        return redirect()->route('listarDocumentos');
    }
}
