<?php

namespace App\Http\Controllers;

use App\Departamento;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Disciplina;
use Illuminate\Support\Facades\DB;

class DisciplinaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('perfil.coordenador');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function listar()
    {
        $disciplinas = Disciplina::all();
        return view('disciplinas.listar', compact('disciplinas'));
    }

    public function incluir()
    {
        $departamentos = Departamento::all();
        return view('disciplinas.incluir', compact('departamentos'));
    }

    public function salvar(Request $rq)
    {
        $dados = $rq->all();

        if(isset($dados['prova'])) {
            $dados['prova'] = TRUE;
        } else {
            $dados['prova'] = FALSE;
        }

        Disciplina::create($dados);

       return redirect()->route('listarDisciplinas');
    }

    public function editar($id)
    {
        $disciplina = Disciplina::find($id);
        $departamentos = Departamento::all();
        return view('disciplinas.editar', compact('disciplina','departamentos'));
    }

    public function atualizar(Request $rq, $id)
    {
        $dados = $rq->all();

        if(isset($dados['prova'])) {
            $dados['prova'] = TRUE;
        } else {
            $dados['prova'] = FALSE;
        }

        Disciplina::find($id)->update($dados);

        return redirect()->route('listarDisciplinas');
    }

    public function deletar($id)
    {
        Disciplina::find($id)->delete();

        return redirect()->route('listarDisciplinas');
    }
}
