<?php

namespace App\Http\Controllers;

use App\Departamento;
use Illuminate\Http\Request;

class DepartamentoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('perfil.administrativo');
    }

    public function listar()
    {
        $departamentos = Departamento::all();
        return view('departamentos.listar', compact('departamentos'));
    }

    public function incluir()
    {
        $departamentos = Departamento::all();
        return view('departamentos.incluir', compact('departamentos'));
    }

    public function salvar(Request $rq)
    {
        $dados = $rq->all();

        Departamento::create($dados);

        return redirect()->route('listarDepartamentos');
    }

    public function editar($id)
    {
        $departamento = Departamento::find($id);
        return view('departamentos.editar', compact('departamento'));
    }

    public function atualizar(Request $rq, $id)
    {
        $dados = $rq->all();

        Departamento::find($id)->update($dados);

        return redirect()->route('listarDepartamentos');
    }

    public function deletar($id)
    {
        Departamento::find($id)->delete();

        return redirect()->route('listarDepartamentos');
    }

}
