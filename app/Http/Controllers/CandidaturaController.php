<?php

namespace App\Http\Controllers;

use App\Candidatura;
use App\Departamento;
use App\Disciplina;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class CandidaturaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('perfil.coordenador')->only('visualizar');
    }

    public function listar()
    {
        $candidaturas = Candidatura::where('aluno', '=', Auth::user()->id)->get();
        return view('candidaturas.listar', compact('candidaturas'));
    }

    public function visualizar()
    {
        $candidaturas = Candidatura::all();
        return view('candidaturas.visualizar', compact('candidaturas'));
    }

    public function incluir()
    {
        $disciplinas = Disciplina::where('vagas', '>', 0)->get();
        $departamentos = Departamento::all();
        return view('candidaturas.incluir', compact('disciplinas', 'departamentos'));
    }

    public function salvar(Request $rq)
    {

        $dados = $rq->all();

        $dados['aluno'] = Auth::user()->id;
        $dados['status'] = 1;

        $candidaturaDuplicada = Candidatura::where('disciplina', $dados['disciplina'])->where('aluno', $dados['aluno'])->first();

        if($candidaturaDuplicada !== null) {
          return redirect()->back()->withErrors(['Você já possui uma candidatura para a disciplina ' . Disciplina::find($dados['disciplina'])->nome]);
        }

        if(!isset($dados['tipo'])) {
            return redirect()->back()->withErrors(['Selecione um tipo de candidatura: Voluntário ou Bolsista.']);
        }

        if(!$rq->hasFile('file') OR !$rq->file('file')->isValid()) {
            return redirect()->back()->withErrors(['É necessário anexar seu histórico para se candidatar.']);
        }

        if($rq->file('file')->getSize() === 0) {
            return redirect()->back()->withErrors(['O arquivo não pode estar vazio.']);
        }

        $historico = $rq->file('file');
        $dados['historico'] = $historico->store('historicos');
        if(!$dados['historico']){
            return redirect()->back()->withErrors(['Erro no upload do histórico. Tente novamente.']);
        }


        if($dados['tipo'] === "voluntario") {
            $dados['voluntario'] = TRUE;
        } else {
            $dados['voluntario'] = FALSE;
        }

        Candidatura::create($dados);

        return redirect()->route('listarCandidaturas');
    }

    public function editar(Request $rq, $id)
    {
        $candidatura = Candidatura::find($id);
        $disciplinas = Disciplina::all();
        $departamentos = Departamento::all();
        return view('candidaturas.editar', compact('candidatura','disciplinas','departamentos'));
    }

    public function atualizar(Request $rq, $id)
    {
        $dados = $rq->all();

        $arquivo = Candidatura::find($id)->historico;

        if(!isset($dados['tipo'])) {
            return redirect()->back()->withErrors(['Selecione um tipo de candidatura: Voluntário ou Bolsista.']);
        }

        if($rq->hasFile('file')) {
          if(!$rq->file('file')->isValid()) {
              return redirect()->back()->withErrors(['É necessário anexar seu histórico para se candidatar.']);
          }

          if($rq->file('file')->getSize() === 0) {
              return redirect()->back()->withErrors(['O arquivo não pode estar vazio.']);
          }

          $historico = $rq->file('file');
          $dados['historico'] = $historico->store('historicos');
          if(!$dados['historico']){
              return redirect()->back()->withErrors(['Erro no upload do histórico. Tente novamente.']);
          }

          Storage::delete($arquivo);
        }

        if(isset($dados['voluntario'])) {
            $dados['voluntario'] = FALSE;
        } else {
            $dados['voluntario'] = TRUE;
        }


        Candidatura::find($id)->update($dados);

        return redirect()->route('listarCandidaturas');
    }

    public function deletar($id)
    {
        $arquivo = Candidatura::find($id)->historico;
        Candidatura::find($id)->delete();
        Storage::delete($arquivo);

        return redirect()->route('listarCandidaturas');
    }
}
