<?php

namespace App\Http\Middleware;

use Closure;

class Administrativo
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user() AND auth()->user()->type == 3) {
            return $next($request);
        }

        return redirect('home');
    }

}
