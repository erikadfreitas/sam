<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidatura extends Model
{
    protected $fillable = [
        'aluno', 'disciplina', 'status', 'voluntario', 'historico'
    ];

    public function getDisciplina()
    {
        return $this->hasOne('App\Disciplina','id', 'disciplina');
    }

    public function getAluno()
    {
        return $this->hasOne('App\User','id', 'aluno');
    }
}
