<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Disciplina extends Model
{
    protected $fillable = [
        'codigo', 'nome', 'professor', 'vagas', 'prova', 'departamento'
    ];

    public function getDepartamento()
    {
        return $this->hasOne('App\Departamento','id', 'departamento');
    }
}
