<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

//Route::get('/', 'DisciplinaController@listar')->name('listarDisciplinas');
Route::get('/', 'HomeController@index')->name('home');

Route::get('/teste', function () {
    return view('teste');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/departamentos', 'DepartamentoController@listar')->name('listarDepartamentos');
Route::get('/departamentos/incluir', 'DepartamentoController@incluir')->name('incluirDepartamento');
Route::post('/departamentos/salvar', 'DepartamentoController@salvar')->name('salvarDepartamento');
Route::get('/departamentos/editar/{id}', 'DepartamentoController@editar')->name('editarDepartamento');
Route::put('/departamentos/atualizar/{id}', 'DepartamentoController@atualizar')->name('atualizarDepartamento');
Route::get('/departamentos/deletar/{id}', 'DepartamentoController@deletar')->name('deletarDepartamento');

Route::get('/disciplinas', 'DisciplinaController@listar')->name('listarDisciplinas');
Route::get('/disciplinas/incluir', 'DisciplinaController@incluir')->name('incluirDisciplina');
Route::post('/disciplinas/salvar', 'DisciplinaController@salvar')->name('salvarDisciplina');
Route::get('/disciplinas/editar/{id}', 'DisciplinaController@editar')->name('editarDisciplina');
Route::put('/disciplinas/atualizar/{id}', 'DisciplinaController@atualizar')->name('atualizarDisciplina');
Route::get('/disciplinas/deletar/{id}', 'DisciplinaController@deletar')->name('deletarDisciplina');

Route::get('/candidaturas', 'CandidaturaController@listar')->name('listarCandidaturas');
Route::get('/candidaturas/visualizar', 'CandidaturaController@visualizar')->name('visualizarCandidaturas');
Route::get('/candidaturas/incluir', 'CandidaturaController@incluir')->name('incluirCandidatura');
Route::post('/candidaturas/salvar', 'CandidaturaController@salvar')->name('salvarCandidatura');
Route::get('/candidaturas/editar/{id}', 'CandidaturaController@editar')->name('editarCandidatura');
Route::put('/candidaturas/atualizar/{id}', 'CandidaturaController@atualizar')->name('atualizarCandidatura');
Route::get('/candidaturas/deletar/{id}', 'CandidaturaController@deletar')->name('deletarCandidatura');

Route::get('/documentos', 'DocumentoController@listar')->name('listarDocumentos');
Route::get('/documentos/visualizar', 'DocumentoController@visualizar')->name('visualizarDocumentos');
Route::get('/documentos/incluir', 'DocumentoController@incluir')->name('incluirDocumento');
Route::post('/documentos/salvar', 'DocumentoController@salvar')->name('salvarDocumento');
Route::get('/documentos/editar/{id}', 'DocumentoController@editar')->name('editarDocumento');
Route::put('/documentos/atualizar/{id}', 'DocumentoController@atualizar')->name('atualizarDocumento');
Route::get('/documentos/deletar/{id}', 'DocumentoController@deletar')->name('deletarDocumento');
